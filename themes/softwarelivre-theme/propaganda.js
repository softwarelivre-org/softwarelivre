jQuery(function($) {

  function fisherYates(myArray) {
    // http://sedition.com/perl/javascript-fy.html
    var temp, i = myArray.length;
    if ( i == 0 ) return false;
    while ( --i ) {
       var j = Math.floor( Math.random() * ( i + 1 ) );
       temp = myArray[i];
       myArray[i] = myArray[j];
       myArray[j] = temp;
    }
  }

  var usedBanners = [];

  window.getBannerData =
  function getBannerData(width, height, placeClass) {
    var lastOption;
    // Impedirá que um banner se repita:
    var request = Math.random();
    // TODO: /plugins/mkt/banner/placeClass/<width>/<height>?request=<request>
    if (!placeClass) placeClass = 'generic'
    var bannerList = propagandaBanners.images[width+'x'+height];
    if (!bannerList) bannerList = [];
    var banners = { target:[], generic:[] };
    for (var b,i=0; b=bannerList[i]; i++) {
      if ( b.target == placeClass ) banners.target.push(b);
      if ( !b.target || b.target == 'generic' ) banners.generic.push(b);
    }
    fisherYates(banners.target);
    fisherYates(banners.generic);

    // try to select a banner planned to the current target.
    // if nothing, try to get a generic.
    for ( type in banners ) {
      bannerList = banners[type];
      while (
        bannerList.length > 0 &&
        usedBanners.indexOf(bannerList[0].src) > -1
      ) lastOption = bannerList.shift();
      
      if ( bannerList[0] ) {
        usedBanners.push( bannerList[0].src );
        return bannerList[0]
      }
    }
    if (!lastOption) lastOption = {};
    return lastOption;
  }

  function createBanner(width, height, placeClass) {
    var banner = getBannerData(width, height, placeClass);
    console.log(width, height, placeClass, banner);
    return $('<a class="propaganda-banner" style="width:'+width+'px; height:'+height+'px;'+
             'display:block; margin: 5px auto 10px; overflow:hidden"'+
             ' target="_blank" href="'+banner.link+'">'+
             '<img src="http://softwarelivre.org'+banner.src+'" alt="propaganda" title="">'+
             '</a>');
  }

  function addHomeBanner() {
    //createBanner(498, 70, 'nicePlace').insertBefore('#highlighted-news > .highlighted-news-item:last-child > div');
    createBanner(498, 70, 'nice-place').appendTo('#highlighted-news > .highlighted-news-item:last-child > div');
    //createBanner(190, 133).insertBefore('.box-2 .block:last-child');
    createBanner(190, 133, 'right-top').insertAfter('.box-3 .block:first-child');
    createBanner(190, 133, 'right-middle').insertAfter('.box-3 .block:nth-child(2)');
    var div0 = $('<div style="text-align:center"></div>').appendTo('#main-content-wrapper-8');
    var div1 = $('<div style="display:inline-block; width:244px; margin-right:10px"></div>').appendTo(div0);
    var div2 = $('<div style="display:inline-block; width:244px"></div>').appendTo(div0);
    createBanner(244, 70, 'the-end').appendTo(div1);
    createBanner(244, 70, 'the-end').appendTo(div2);
  }

  function addNewsBanner() {
    //createBanner(498, 70, 'nicePlace').insertAfter('#article .article-body p:nth-child(2)');
    createBanner(498, 70, 'bottonNews').appendTo('#article .article-body');
    createBanner(190, 133).insertAfter('.box-2 .block:nth-child(2)');
    createBanner(190, 133).insertAfter('.box-3 .block:first-child');
  }

  if ( $(document.body).hasClass('controller-home action-home-index') ) {
    addHomeBanner();
  } else {
    if (
      $(document.body).hasClass('controller-content_viewer') &&
      true // /softwarelivre\.org\/portal/.test(document.location.href)
    ) {
      addNewsBanner();
    }
  }

});
