/* Banners dos apoios */
var goAPOIO = document.getElementById("go-apoio");
goAPOIO.bgY = 0;
function APOIOCicle(cont) {
  if ( cont ) { goAPOIO.stop = false }
  goAPOIO.bgY--;
  if ( goAPOIO.bgY < 0 ) { goAPOIO.bgY = 370 }
  goAPOIO.style.backgroundPosition = "0px "+goAPOIO.bgY+"px"
  if ( ! goAPOIO.stop ) { setTimeout( "APOIOCicle()", 60 ) }
}
APOIOCicle();

/* Google Analytics */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-26286704-1']);
_gaq.push(['_trackPageview']);
(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

/* Redirecionar perfil /ASL -> /asl */
jQuery(function($) {
  if (document.location.pathname == '/ASL') {
    $('#not-found').html('<h1>redirecionando...</h1>');
    document.location.href = '/asl';
  }
});
